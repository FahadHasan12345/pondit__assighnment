package com.example.icc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.icc.model.CoachingStaff;
import com.example.icc.service.CoachingStaffService;

/*************************************************************************
 * {@link CoachingStaff} Controller
 * 
 * @author Fahad Hasan
 * @since 2020-11-18
 *************************************************************************/
@RestController
@RequestMapping("/icc/coachingStaff")
public class CoachingStaffController {

	@Autowired
	private CoachingStaffService service;

	/*************************************************************************
	 * Create a new CoachingStaff
	 * 
	 * @param ob {@link CoachingStaff} object
	 * @param rs {@link HttpServletResponse} object
	 * @return {@link CoachingStaff}
	 *************************************************************************/
	@PostMapping
	public CoachingStaff create(@Valid @RequestBody CoachingStaff ob, HttpServletRequest rq, HttpServletResponse rs) {
		rs.setStatus(HttpServletResponse.SC_CREATED);
		return service.saveCoachingStaff(ob, rs);
	}

	/*************************************************************************
	 * Get all {@link CoachingStaff}
	 * 
	 * @return {@link List<CoachingStaff>}
	 *************************************************************************/
	@GetMapping("/getAll/byCountry/{countryId}")
	public List<CoachingStaff> getAll(@PathVariable Long countryId) {
		return service.getAllByCountry(countryId);
	}

	/*************************************************************************
	 * Update {@link CoachingStaff}
	 * 
	 * @param ob {@link CoachingStaff} object
	 * @return {@link CoachingStaff}
	 *************************************************************************/
	@PutMapping
	public CoachingStaff update(@Valid @RequestBody CoachingStaff ob, HttpServletRequest rq, HttpServletResponse rs) {
		return service.update(ob, rs);
	}

	/*************************************************************************
	 * Delete {@link CoachingStaff}
	 * 
	 * @param id Id of CoachingStaff
	 * @return
	 *************************************************************************/
	@DeleteMapping
	public String delete(@RequestBody Long id) {
		return service.delete(id);
	}

}
