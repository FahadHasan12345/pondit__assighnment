package com.example.icc.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.icc.model.Player;
import com.example.icc.service.PlayerService;

/*************************************************************************
 * {@link Player} Controller
 * 
 * @author Fahad Hasan
 * @since 2020-11-18
 *************************************************************************/
@RestController
@RequestMapping("/icc/player")
public class PlayerController {

	@Autowired
	private PlayerService service;

	/*************************************************************************
	 * Create a new Player
	 * 
	 * @param ob {@link Player} object
	 * @param rs {@link HttpServletResponse} object
	 * @return {@link Player}
	 *************************************************************************/
	@PostMapping
	public Player create(@Valid @RequestBody Player ob, HttpServletRequest rq, HttpServletResponse rs) {
		rs.setStatus(HttpServletResponse.SC_CREATED);
		return service.savePlayer(ob, rs);
	}

	/*************************************************************************
	 * Get all {@link Player}
	 * 
	 * @return {@link List<Player>}
	 *************************************************************************/
	@GetMapping("/getAll/byCountry/{countryId}")
	public List<Player> getAll(@PathVariable Long countryId) {
		return service.getAllByCountry(countryId);
	}

	/*************************************************************************
	 * Update {@link Player}
	 * 
	 * @param ob {@link Player} object
	 * @return {@link Player}
	 *************************************************************************/
	@PutMapping
	public Player update(@Valid @RequestBody Player ob, HttpServletRequest rq, HttpServletResponse rs) {
		return service.update(ob, rs);
	}

	/*************************************************************************
	 * Delete {@link Player}
	 * 
	 * @param id Id of Player
	 * @return
	 *************************************************************************/
	@DeleteMapping
	public String delete(@RequestBody Long id) {
		return service.delete(id);
	}

}
