package com.example.icc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.icc.model.User;
import com.example.icc.service.UserService;

/*************************************************************************
 * {@link User} Controller
 * 
 * @author Fahad Hasan
 * @since 2020-11-18
 *************************************************************************/
@RestController
@RequestMapping("/icc/User")
public class UserController {

	@Autowired
	private UserService service;

	/*************************************************************************
	 * Create a new User
	 * 
	 * @param ob {@link User} object
	 * @param rs {@link HttpServletResponse} object
	 * @return {@link User}
	 *************************************************************************/
	@PostMapping
	public User create(@Valid @RequestBody User ob, HttpServletRequest rq, HttpServletResponse rs) {
		rs.setStatus(HttpServletResponse.SC_CREATED);
		return service.saveUser(ob, rs);
	}

	/*************************************************************************
	 * Get all {@link User}
	 * 
	 * @return {@link List<User>}
	 *************************************************************************/
	@GetMapping("/getAll/")
	public List<User> getAll() {
		return service.getAll();
	}
	
	/*************************************************************************
	 * Update {@link User}
	 * 
	 * @param ob {@link User} object
	 * @return {@link User}
	 *************************************************************************/
	@PutMapping
	public User update(@Valid @RequestBody User ob, HttpServletRequest rq, HttpServletResponse rs) {
		return service.update(ob, rs);
	}

	/*************************************************************************
	 * Delete {@link User}
	 * 
	 * @param id Id of User
	 * @return
	 *************************************************************************/
	@DeleteMapping
	public String delete(@RequestBody Long id) {
		return service.delete(id);
	}
}
