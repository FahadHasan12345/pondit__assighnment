package com.example.icc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.icc.model.Country;
import com.example.icc.service.CountryService;

/*************************************************************************
 * {@link Country} Controller
 * 
 * @author Fahad Hasan
 * @since 2020-11-18
 *************************************************************************/
@RestController
@RequestMapping("/icc/Country")
public class CountryController {

	@Autowired
	private CountryService service;

	/*************************************************************************
	 * Create a new Country
	 * 
	 * @param ob {@link Country} object
	 * @param rs {@link HttpServletResponse} object
	 * @return {@link Country}
	 *************************************************************************/
	@PostMapping
	public Country create(@Valid @RequestBody Country ob, HttpServletRequest rq, HttpServletResponse rs) {
		rs.setStatus(HttpServletResponse.SC_CREATED);
		return service.saveCountry(ob, rs);
	}

	/*************************************************************************
	 * Get all {@link Country}
	 * 
	 * @return {@link List<Country>}
	 *************************************************************************/
	@GetMapping("/getAll/")
	public List<Country> getAll() {
		return service.getAll();
	}

	/*************************************************************************
	 * Delete {@link Country}
	 * 
	 * @param id Id of Country
	 * @return
	 *************************************************************************/
	@DeleteMapping
	public String delete(@RequestBody Long id) {
		return service.delete(id);
	}
}
