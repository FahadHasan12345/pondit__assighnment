package com.example.icc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.icc.model.Country;
import com.example.icc.model.Player;

public interface PlayerRepo extends JpaRepository<Player, Long> {

	List<Player> getAllPlayerByCountry(Country country);
}
