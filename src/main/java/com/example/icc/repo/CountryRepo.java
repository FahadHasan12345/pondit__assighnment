package com.example.icc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.icc.model.Country;

public interface CountryRepo extends JpaRepository<Country, Long> {



}
