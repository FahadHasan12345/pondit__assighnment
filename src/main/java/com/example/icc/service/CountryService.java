package com.example.icc.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.icc.model.CoachingStaff;
import com.example.icc.model.Country;
import com.example.icc.model.Player;
import com.example.icc.repo.CoachingStaffRepo;
import com.example.icc.repo.CountryRepo;
import com.example.icc.repo.PlayerRepo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryService {

	PlayerRepo playerRepo;
	CountryRepo repo;
	CoachingStaffRepo coachingStaffRepo;

	/*************************************************************************
	 * Create a new Country
	 * 
	 * @param ob {@link Country} object
	 * @return {@link Country}
	 *************************************************************************/
	public Country saveCountry(Country ob, HttpServletResponse rs) {
		try {
			return repo.save(ob);
		} catch (Exception e) {
			rs.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return ob;
		}
	}

	/*************************************************************************
	 * Get all {@link Country}
	 * 
	 * @return {@link Country}
	 *************************************************************************/
	public List<Country> getAll() {
		return repo.findAll();
	}

	/*************************************************************************
	 * Delete {@link Country}
	 * 
	 * @param id Id of Country
	 * @return
	 *************************************************************************/

	public String delete(Long id) {
		repo.deleteById(id);
		deletePlayers(id);
		deleteCoachingStaff(id);
		return "Deleted Sucessfully!!!!!!!";
	}

	private void deletePlayers(Long id) {

		List<Player> players = playerRepo.getAllPlayerByCountry(repo.findById(id).orElse(null));
		for (Player ob : players) {
			playerRepo.delete(ob);
		}
	}

	private void deleteCoachingStaff(Long id) {
		List<CoachingStaff> coachingStaff = coachingStaffRepo
				.getAllCoachingStaffByCountry(repo.findById(id).orElse(null));
		for (CoachingStaff ob : coachingStaff) {
			coachingStaffRepo.delete(ob);
		}
	}

}
