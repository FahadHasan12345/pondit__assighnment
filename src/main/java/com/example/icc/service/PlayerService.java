package com.example.icc.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.icc.model.Player;
import com.example.icc.repo.CountryRepo;
import com.example.icc.repo.PlayerRepo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PlayerService {

	PlayerRepo repo;
	CountryRepo countryRepo;

	/*************************************************************************
	 * Create a new Player
	 * 
	 * @param ob {@link Player} object
	 * @return {@link Player}
	 *************************************************************************/
	public Player savePlayer(Player ob, HttpServletResponse rs) {
		try {
			ob.setCountry(countryRepo.findById(ob.getCountryId()).orElse(null));
			return repo.save(ob);
		} catch (Exception e) {
			rs.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return ob;
		}
	}

	/*************************************************************************
	 * Get all {@link Player}
	 * 
	 * @return {@link Player}
	 *************************************************************************/
	public List<Player> getAllByCountry(Long countryId) {
		return repo.getAllPlayerByCountry(countryRepo.findById(countryId).get()).stream().peek(ob -> {
			ob.setCountryId(ob.getCountry().getId());
			ob.setCountryName(ob.getCountry().getName());
		}).collect(Collectors.toList());
	}

	/*************************************************************************
	 * Update {@link Player}
	 * 
	 * @param ob {@link Player} object
	 * @return {@link Player}
	 *************************************************************************/

	public Player update(Player ob, HttpServletResponse rs) {
		try {
			ob.setCountry(countryRepo.findById(ob.getCountryId()).orElse(null));
			return repo.save(ob);
		} catch (Exception e) {
			rs.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return ob;
		}
	}

	/*************************************************************************
	 * Delete {@link Player}
	 * 
	 * @param id Id of Player
	 * @return
	 *************************************************************************/

	public String delete(Long id) {
		repo.deleteById(id);
		return "Deleted Sucessfully!!!!!!!";
	}

}
