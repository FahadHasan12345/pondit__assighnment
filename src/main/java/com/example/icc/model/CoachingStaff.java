package com.example.icc.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

/**
 * The persistent class for the icc database table.
 *
 * @author Fahad Hasan
 * 
 * @since 2020-11-17
 */

@Data
@Entity
@Table(name = "icc_coaching_staff")
public class CoachingStaff {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(value = AccessLevel.PRIVATE)
	@Column(updatable = false, nullable = false)
	private Long id;
	
	@NotNull
	private String name;
	
	@NotNull
	private int age;
	
	@NotNull
	private String coachingType;
	
	@Column(nullable = false)
	@NonNull
	private LocalDate creationDate = LocalDate.now();
	
	@ManyToOne
	@JoinColumn(name = "country_id",referencedColumnName = "id", nullable = false)
	@JsonIgnore
	private Country country;
	
	@Transient
	@NotNull
	private Long countryId;
	
	@Transient
	@NotNull
	private String countryName;

}
