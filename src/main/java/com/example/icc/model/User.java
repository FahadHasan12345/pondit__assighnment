package com.example.icc.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

/**
 * The persistent class for the icc database table.
 *
 * @author Fahad Hasan
 * 
 * @since 2020-11-17
 */

@Data
@Entity
@Table(name = "icc_users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(value = AccessLevel.PRIVATE)
	@Column(updatable = false, nullable = false)
	private Long id;
	
	@NotNull
	private String name;
	
	@NotNull
	@Email
	private String email;
	
	@NotNull
	private String password;
	
	@NotNull
	private int age;
	
	@Column(nullable = false)
	@NonNull
	private LocalDate creationDate = LocalDate.now();

}
